class NewClass {
  constructor(name) {
    this.name = name;
    this.arr = [name];
  }
  get getName() {
    return this.name;
  }

  set changeName(value) {
    this.arr.push(value);
    this.name = value;
    return this.name;
  }

  undo() {
    this.arr.pop();
    let lastElement = this.arr.length - 1;
    this.name = this.arr[lastElement];
    return this.name;
  }
}

let data = new NewClass("Somebody");
console.log("getter -> ", data.name);

data.changeName = "John"
console.log("setter ->", data.name);

data.undo();
console.log("undo ->", data.name);
